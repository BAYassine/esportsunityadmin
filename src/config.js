export default {
  auth: {
    email: 'admin@esu.com',
    password: ''
  },
  API_URL : process.env.REACT_APP_API_URL,
  NODE_URL : process.env.REACT_APP_SERVER_URL
};
