export const StageFormats = {
    SINGLE_ELIMINATION : "Single Elimination",
    ROUND_ROBIN : "Round Robin",
    LEAGUE : "League",
    DOUBLE_ELIMINATION : "Double Elimination",
    SWISS_SYSTEM : "Swiss System"
}

export const TournamentStatus = {
    REGISTRATION : "Registration",
    STARTING : "Starting",
    STARTED : "Started",
    GROUP_STAGE : "Group Stage",
    PLAYOFFS : "PlayOffs",
    SEMI_FINALS : "Semi Finals",
    FINALS : "Finals"
}