import axios from 'axios';
import { logoutUser } from '../actions/user';
import store from '../reducers';

const { REACT_APP_API_URL } = process.env;

const BaseAxios = axios.create({
    baseURL : REACT_APP_API_URL
});

const AuthAxios = axios.create({
    baseURL : REACT_APP_API_URL
});

AuthAxios.interceptors.request.use(function (config) {
    let auth = localStorage.getItem('auth')
    if(auth == null){
        window.location.href = "/login";
        return Promise.reject(config);
    }
    const { token } = JSON.parse(auth)
    config.headers['Authorization'] = "Bearer " + token
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

AuthAxios.interceptors.response.use((response) => {
    return response;
  }, function (error) {  
    if(!error.response || error.response.status == 401){
        store.dispatch(logoutUser());
        window.location.href = "/login";
    }
    return Promise.reject(error);
  });

export { AuthAxios, BaseAxios };