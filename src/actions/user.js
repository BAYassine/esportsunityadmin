export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

export function receiveLogin(payload) {
    return {
        type: LOGIN_SUCCESS,
        payload
    };
}

export function loginError(payload) {
    return {
        type: LOGIN_FAILURE,
        payload,
    };
}

function requestLogout() {
    return {
        type: LOGOUT_REQUEST,
    };
}

export function receiveLogout() {
    return {
        type: LOGOUT_SUCCESS,
    };
}

// Logs the user out
export function logoutUser() {
    return (dispatch) => {
        dispatch(requestLogout());
        localStorage.removeItem('authenticated');
        localStorage.removeItem('auth');
        dispatch(receiveLogout());
    };
}

export function loginUser({ token, expiresIn, ...user }) {
    return (dispatch) => {
        localStorage.setItem('authenticated', true)
        localStorage.setItem('user', JSON.stringify(user))
        localStorage.setItem('auth', JSON.stringify({ token, expiresIn }))
        dispatch(receiveLogin(user));
    }
}
