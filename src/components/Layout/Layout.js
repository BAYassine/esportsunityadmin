import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, withRouter, Redirect, useRouteMatch } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import Hammer from 'rc-hammerjs';

import Dashboard from '../../pages/dashboard';

import Header from '../Header';
import Sidebar from '../Sidebar';
import BreadcrumbHistory from '../BreadcrumbHistory';
import { openSidebar, closeSidebar } from '../../actions/navigation';
import s from './Layout.module.scss';
import Teams from '../../pages/teams/Teams';
import TeamForm from '../../pages/teams/TeamForm';
import Static from '../../pages/tables/static/Static';
import Games from '../../pages/games/Games';
import GameForm from '../../pages/games/GameForm';
import NewsForm from '../../pages/news/NewsForm';
import NewsTable from '../../pages/news/NewsTable';
import TournamentsForm from '../../pages/Tournaments/TournamentsForm';
import TournamentsTable from '../../pages/Tournaments/TournamentsTable';
import TournamentDetails from '../../pages/Tournaments/TournamentDetails';

const Layout = (props) => {

  const [chatOpen, setChatOpen] = useState(false);

  const { path } = useRouteMatch();

  const handleSwipe = (e) => {
    if ('ontouchstart' in window) {
      if (e.direction === 4 && !chatOpen) {
        props.dispatch(openSidebar());
        return;
      }

      if (e.direction === 2 && props.sidebarOpened) {
        props.dispatch(closeSidebar());
        return;
      }

      setChatOpen(e.direction === 2);
    }
  }

  return (
    <div
      className={[
        s.root,
        'sidebar-' + props.sidebarPosition,
        'sidebar-' + props.sidebarVisibility,
      ].join(' ')}
    >
      <div className={s.wrap}>
        <Header />
        {/* <Chat chatOpen={this.state.chatOpen} /> */}
        {/* <Helper /> */}
        <Sidebar />
        <Hammer onSwipe={handleSwipe}>
          <main className={s.content}>
            <BreadcrumbHistory url={props.location.pathname} />
            <TransitionGroup>
              <CSSTransition
                key={props.location.key}
                classNames="fade"
                timeout={200}
              >
                <Switch>
                  <Route path={`${path}teams/create`} component={TeamForm} />
                  <Route path={`${path}teams`} component={Teams} />
                  <Route path={`${path}games/create`} component={GameForm} />
                  <Route path={`${path}games`} component={Games} />
                  <Route path={`${path}news/create`} component={NewsForm} />
                  <Route path={`${path}news`} component={NewsTable} />
                  <Route path={`${path}tournaments/create`} component={TournamentsForm} />
                  <Route path={`${path}tournaments/:id`} component={TournamentDetails} />
                  <Route path={`${path}tournaments`} component={TournamentsTable} />
                  <Route path="/tables" component={Static} />
                  <Route path={`${path}dashboard`} component={Dashboard}/>
                  <Route path={`${path}`} exact render={() => <Redirect to="/dashboard" />} />
                </Switch>
              </CSSTransition>
            </TransitionGroup>
            <footer className={s.contentFooter}>
              Light Blue React Template - React admin template made by <a href="https://flatlogic.com" >Flatlogic</a>
            </footer>
          </main>
        </Hammer>
      </div>
    </div>
  );
}

function mapStateToProps(store) {
  return {
    sidebarOpened: store.navigation.sidebarOpened,
    sidebarPosition: store.navigation.sidebarPosition,
    sidebarVisibility: store.navigation.sidebarVisibility,
  };
}

export default withRouter(connect(mapStateToProps)(Layout));
