import { combineReducers } from 'redux';
import auth from './auth';
import navigation from './navigation';
import alerts from './alerts';
import register from './register';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk'

const reducers = combineReducers({
  alerts,
  auth,
  navigation,
  register,
});


const store = createStore(
  reducers,
  applyMiddleware(ReduxThunk)
);

export default store;