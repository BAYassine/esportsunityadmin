import {
     LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT_SUCCESS,
} from '../actions/user';

const authenticated = localStorage.getItem('authenticated');
const userJSON = localStorage.getItem('user');
export default function auth(state = {
    user : userJSON ? JSON.parse(userJSON) : null,
    isFetching: false,
    isAuthenticated: authenticated,
}, action) {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return Object.assign({}, state, {
                user : action.payload,
                isFetching: false,
                isAuthenticated: true,
                errorMessage: '',
            });
        case LOGIN_FAILURE:
            return Object.assign({}, state, {
                isFetching: false,
                isAuthenticated: false,
                errorMessage: action.payload,
            });
        case LOGOUT_SUCCESS:
            return Object.assign({}, state, {
                user : null,
                isAuthenticated: false,
            });
        default:
            return state;
    }
}
