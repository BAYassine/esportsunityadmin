import React, { useEffect, useState } from 'react';
import {
  Row,
  Col,
  Table,
  Button,
  UncontrolledButtonDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
} from "reactstrap";

import Widget from "../../components/Widget";
import s from"./Teams.module.scss";
import axios from 'axios';
import config from '../../config';
import { Link, useRouteMatch } from 'react-router-dom';

const Teams = () => {

  const { url } = useRouteMatch();
  const [teams, setTeams] = useState(null);

  useEffect(() => {
    axios.get(`${config.API_URL}/admin/teams/list`)
      .then(({ data }) => setTeams(data));
  }, []);

  const longText = (text) => {
    return text && text.substr(0, 100) + (text.length > 100 ? '...' : '');
  }

  return (
    <div className={s.root}>
      <div className="d-flex justify-content-between">
        <h2 className="page-title">
          Teams - <span className="fw-semi-bold">List</span>
        </h2>
        <Link to={url + "/create"} >
          <Button color="default"><i className="fa fa-plus mr-3 ml-2"></i>Create new team</Button>
        </Link>
      </div>
      <Row>
        <Col>
          <Widget
            settings
            close
            bodyClass={s.mainTableWidget}
          >
            <Table striped>
              <thead>
                <tr className="fs-sm">
                  <th className="hidden-sm-down">#</th>
                  <th>Logo</th>
                  <th className="hidden-sm-down">Name</th>
                  <th className="hidden-sm-down">Team Tag</th>
                  <th>About</th>
                  <th className="hidden-sm-down">Members</th>
                  <th className="hidden-sm-down">CEO</th>
                  {/* <th className="hidden-sm-down">Status</th> */}
                </tr>
              </thead>
              <tbody>
                {teams && teams.map((row, i) => (
                  <tr key={row.id}>
                    <td>{i + 1}</td>
                    <td>
                      <img
                        className="img-rounded"
                        src={`${process.env.REACT_APP_SERVER_URL}${row.logo}`}
                        alt=""
                        height="50"
                      />
                    </td>
                    <td>
                      {row.name}
                    </td>
                    <td>
                      {row.tag}
                    </td>
                    <td style={{ maxWidth : "300px" }}>
                      <p className="mb-0">
                        {longText(row.about)}
                      </p>
                    </td>
                    <td className="text-muted">{row.members && row.members.length}</td>
                    <td className="text-muted">{row.ceo}</td>
                    {/* <td className="width-150"></td> */}
                  </tr>
                ))}
              </tbody>
            </Table>
            <div className="clearfix">
              <div className="float-right">
                <Button color="default" className="mr-2" size="sm">
                  Send to...
                  </Button>
                <UncontrolledButtonDropdown>
                  <DropdownToggle
                    color="inverse"
                    className="mr-xs"
                    size="sm"
                    caret
                  >
                    Clear
                    </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem>Clear</DropdownItem>
                    <DropdownItem>Move ...</DropdownItem>
                    <DropdownItem>Something else here</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>Separated link</DropdownItem>
                  </DropdownMenu>
                </UncontrolledButtonDropdown>
              </div>
              <p>Basic table with styled content</p>
            </div>
          </Widget>
        </Col>
      </Row>

    </div>
  )
}

export default Teams;