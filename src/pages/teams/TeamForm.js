import React, { useCallback, useState } from 'react';
import { Col, Row, Form, Button, FormGroup, FormText, Input, Label } from 'reactstrap';
import { useDropzone } from 'react-dropzone';
import s from "./Teams.module.scss";
import Widget from '../../components/Widget/Widget';
import axios from 'axios';
import config from '../../config';
import { useHistory } from 'react-router-dom';

const TeamForm = () => {

    const history = useHistory();

    const [team, setTeam] = useState({
        name : null,
        symbol : null,
        about : null,
        country : null,
        ceo : null,
        foundation : null,
        website : null
    })

    const onDrop = useCallback(acceptedFiles => {
        // Do something with the files
    }, [])
    const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop })

    const handleChange = (e) => {
        setTeam({ ...team, [e.target.name] : e.target.value })
    }

    const submit = () => {
        axios.post(`${config.API_URL}/teams/create`, team)
            .then(() => history.push('/admin/teams'))
    }

    return (
        <>
            <h2 className="page-title">
                Teams - <span className="fw-semi-bold">Create new team</span>
            </h2>
            <Widget title={
                <h5 className="mb-2">
                    Team <span className="fw-semi-bold">Informations</span>
                </h5>
            }>
                <div className="d-flex mb-5">
                    <div className={s.dropzone} {...getRootProps()}>
                        <input {...getInputProps()} />
                        {
                            <p>Drag a logo here</p>
                        }
                    </div>
                    <div style={{ flex: 3 }}>
                        <Row>
                            <Col md={9}>
                                <FormGroup controlId="team-name">
                                    <Label>Team name</Label>
                                    <Input name="name" size="lg" type="text" placeholder="Enter name" className={s.teamname_field} onChange={handleChange}/>
                                </FormGroup>
                            </Col>
                            <Col md={3}>
                                <FormGroup controlId="team-shortname">
                                    <Label>Symbol</Label>
                                    <Input name="symbol" size="lg" type="text"  className={s.teamname_field} onChange={handleChange}/>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup controlId="team-about">
                                    <Label>About</Label>
                                    <Input name="about" size="lg" type="textarea" placeholder="Enter name" className={s.teamabout_field} onChange={handleChange}/>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup controlId="team-ceo">
                                    <Label>CEO</Label>
                                    <Input name="ceo" size="lg" type="text" placeholder="Enter CEO fullname" onChange={handleChange}/>
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup controlId="team-country">
                                    <Label>Country</Label>
                                    <Input name="country" size="lg" type="text" placeholder="Enter Country" onChange={handleChange}/>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup controlId="team-country">
                                    <Label>Foundation date</Label>
                                    <Input name="foundation" size="lg" type="date" placeholder="Enter Country" onChange={handleChange}/>
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup controlId="team-country">
                                    <Label>Website</Label>
                                    <Input name="website" size="lg" type="url" placeholder="Enter team website" onChange={handleChange}/>
                                </FormGroup>
                            </Col>
                        </Row>
                    </div>
                </div>
                <Row>
                    <Col></Col>
                    <Col md={2}>
                        <Button color="primary" type="submit" block onClick={submit}>
                            Submit
                        </Button>
                    </Col>
                </Row>
            </Widget>
        </>
    )
}

export default TeamForm;