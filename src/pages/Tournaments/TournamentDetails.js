import React, { useEffect, useState } from 'react';
import { Link, Switch, Route, useRouteMatch, useParams, Redirect } from 'react-router-dom';
import { Row, Col, TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap"
import Widget from "../../components/Widget/Widget"
import { AuthAxios } from '../../utils/AxiosInstances';
import GeneralTournamentForm from './GeneralForm';
import Matches from './Matches';
import Prizes from './Prizes';
import Stages from './Stages';
import TournamentTeams from './Teams';

const TournamentDetails = () => {

    let { id } = useParams();
    const { path, url } = useRouteMatch();

    const [tournament, setTournament] = useState();
    const [ activeTab, setActiveTab ] = useState('1');

    useEffect(() => {
        AuthAxios.get('admin/tournaments/' + id  + "/details")
            .then(({ data }) => setTournament(data))
    }, [id])

    const onUpdate = () => {
        AuthAxios.get('admin/tournaments/details/' + id)
            .then(({ data }) => setTournament(data))
    }

    return (
        <>
            {tournament && (
                <Row>
                    <Col md={3}>
                        <Widget>
                            <Nav className="flex-column" tabs>
                                <NavItem>
                                    <NavLink onClick={() => setActiveTab('1')} to={`${url}/general`}
                                        className={"nav-link" + (activeTab == '1'? ' active' : '')}>
                                        General
                                    </NavLink>
                                </NavItem>
                                <NavItem className="nav-item">
                                    <NavLink onClick={() => setActiveTab('2')} to={`${url}/stages`}
                                        className={"nav-link" + (activeTab == '2'? ' active' : '')}>
                                        Stages
                                    </NavLink>
                                </NavItem>
                                <NavItem className="nav-item">
                                    <NavLink onClick={() => setActiveTab('3')} to={`${url}/prizes`}
                                        className={"nav-link" + (activeTab == '3'? ' active' : '')}>
                                        Prizes
                                    </NavLink>
                                </NavItem>
                                <NavItem className="nav-item">
                                    <NavLink onClick={() => setActiveTab('4')} to={`${url}/teams`}
                                        className={"nav-link" + (activeTab == '4'? ' active' : '')}>
                                        Teams
                                    </NavLink>
                                </NavItem>
                                <NavItem className="nav-item">
                                    <NavLink onClick={() => setActiveTab('5')} to={`${url}/matches`}
                                        className={"nav-link" + (activeTab == '5'? ' active' : '')}>
                                        Matches
                                    </NavLink>
                                </NavItem>
                            </Nav>
                        </Widget>
                    </Col>
                    <Col md={8}>
                        <TabContent activeTab={activeTab}>
                            <TabPane tabId="1">
                                <GeneralTournamentForm tournament={tournament}/>
                            </TabPane>
                            <TabPane tabId="2">
                                <Stages tournamentId={tournament.id} stages={tournament.stages} onUpdate={onUpdate}/>
                            </TabPane>
                            <TabPane tabId="3">
                                <Prizes tournamentId={tournament.id} prizes={tournament.prizes} onUpdate={onUpdate}/>
                            </TabPane>
                            <TabPane tabId="4">
                                <TournamentTeams tournamentId={tournament.id}/>
                            </TabPane>
                            <TabPane tabId="5">
                                <Matches tournamentId={tournament.id}/>
                            </TabPane>
                        </TabContent>
                    </Col>
                </Row>
            )}
        </>
    )
}

export default TournamentDetails;