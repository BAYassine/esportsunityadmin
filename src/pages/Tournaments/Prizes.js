import React from 'react'
import { useState } from 'react'
import { Col, FormGroup, Row, Button, Input, Alert } from 'reactstrap'
import Label from 'reactstrap/lib/Label'
import Widget from '../../components/Widget/Widget'
import { AuthAxios } from '../../utils/AxiosInstances'

const { REACT_APP_SERVER_URL } = process.env;

const Prizes = ({ prizes, tournamentId, onUpdate }) => {

    const emptyPrize = {
        label : null,
        photo : null,
        from : null,
        to : null
    }

    const [ prize, setPrize ] = useState(emptyPrize)
    const [ photoPreview, setPhotoPreview ] = useState()

    const handleChange = ({ target }) => {
        setPrize({ ...prize, [target.name] : target.value });
    }

    const handleUpload = (event) => {
        const files = event.target.files;
        const name = event.target.name;
        if (files && files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                setPhotoPreview(e.target.result)
            };
            
            const formData = new FormData();
            formData.append('file', files[0]);
            AuthAxios.post('/media/upload', formData)
                .then(({ data }) => {
                    setPrize({ ...prize, [name] : data.name });
                });
            reader.readAsDataURL(files[0]);
        }
    }

    const submit = () => {
        AuthAxios.post('admin/tournaments/' + tournamentId + '/prizes', prize)
            .then(() => {
                onUpdate()
                setPrize(emptyPrize)
            })
    }

    const onRemove = (prizeLabel, prizeId) => {
        if(window.confirm("Do you really on to remove prize "+ prizeLabel)){
            AuthAxios.delete(`admin/tournaments/${tournamentId}/prizes/${prizeId}`)
                .then(onUpdate)
        }
    }

    return (
        <>
            <Widget>
                <h2>New Prize</h2>
                <Row>
                    <div className="prize-photo-preview">
                        <span>Prize Photo</span>
                        {photoPreview && <img src={photoPreview} />}
                    </div>
                    <Col>
                        <FormGroup>
                            <Label>Prize label</Label>
                            <Input name="label" defaultValue={prize.name} onChange={handleChange}/>
                        </FormGroup>
                        <FormGroup>
                            <Label>Photo</Label>
                            <Input name="photo" type="file" onChange={handleUpload}/>
                        </FormGroup>
                    </Col>
                </Row>
                <Label>Prize range <small>(Example from rank 2 to rank 3)</small></Label>
                <Row>
                    <Col md={3}>
                        <FormGroup>
                            <Input placeholder="From rank" type="number" name="from" defaultValue={prize.format} onChange={handleChange}/>
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>
                            <Input placeholder="To rank" type="number" name="to" defaultValue={prize.rounds} onChange={handleChange} />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-right">
                        <Button color="primary" onClick={submit}>Save</Button>
                    </Col>
                </Row>
            </Widget>

            <h2 className="mb-3">Tournament Prizes</h2>
            <Row>
                {prizes.map((prize, i) => ( 
                    <Col md={6} key={i}>
                        <Widget close onClose={() => onRemove(prize.label, prize.id)}>
                            <Row>
                                {prize.photo != null &&
                                    <img src={REACT_APP_SERVER_URL + prize.photo} className="prize-photo"/>
                                }
                                <Col className={prize.photo ? "d-flex flex-column justify-content-center" : ""}>
                                    <h5 className="fw-semi-bold">{prize.label}</h5>
                                    {prize.from != prize.to ?
                                        <h5><b>Range : </b>{prize.from} - {prize.to}</h5>
                                        :
                                        <h5><b>Place  : </b>{prize.from}</h5>
                                    }
                                </Col>
                            </Row>
                        </Widget>
                    </Col>
                ))}
            </Row>
        </>
    )
}

export default Prizes;