import React from 'react'
import { useState } from 'react'
import { Col, FormGroup, Row, Button, Input, Alert, ModalBody, Modal } from 'reactstrap'
import Label from 'reactstrap/lib/Label'
import ModalHeader from 'reactstrap/lib/ModalHeader'
import Widget from '../../components/Widget/Widget'
import { AuthAxios } from '../../utils/AxiosInstances'

const Matches = ({ tournamentId, onUpdate }) => {

    const emptyMatch = {
        name: null,
        format: null,
        rounds: null
    }

    const [match, setMatch] = useState(emptyMatch)
    const [creating, SetCreatingMatch] = useState(false);

    const handleChange = ({ target }) => {
        setMatch({ ...match, [target.name]: target.value });
    }

    const submit = () => {
        AuthAxios.post('admin/tournaments/' + tournamentId + '/match', match)
            .then(() => {
                onUpdate(match)
                setMatch(emptyMatch)
            })
    }

    const onRemove = (matchId) => {
        if (window.confirm("Do you really on to remove Match ")) {
            AuthAxios.delete(`admin/tournaments/${tournamentId}/matches/${matchId}`)
                .then(onUpdate)
        }
    }

    return (
        <>
            <Row>
                <Col>
                    <h2 className="mb-3">Tournament Matchs</h2>
                </Col>
                <Col md={2} className="text-right">
                    <Button color="primary" onClick={() => SetCreatingMatch(true)}>Add Match</Button>
                </Col>
            </Row>
            <Modal isOpen={creating}toggle={() => SetCreatingMatch(false)}>
                <ModalHeader toggle={() => SetCreatingMatch(false)}>
                    <h2>New Match</h2>
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label>Match Name</Label>
                                <Input name="name" defaultValue={match.name} onChange={handleChange} />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label>Match Order</Label>
                                <Input name="order" defaultValue={match.order} onChange={handleChange} />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label>Match Format</Label>
                                <Input type="select" name="format" defaultValue={match.format} onChange={handleChange}>
                                    <option value="SINGLE_ELIMINATION">Single Elimination</option>
                                    <option value="DOUBLE_ELIMINATION">Double Elimination</option>
                                    <option value="ROUND_ROBIN">Round Robin</option>
                                    <option value="LEAGUE">League</option>
                                </Input>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label>Number of rounds</Label>
                                <Input type="number" name="rounds" defaultValue={match.rounds} onChange={handleChange} />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="text-right">
                            <Button color="primary" onClick={submit}>Save</Button>
                        </Col>
                    </Row>
                </ModalBody>
            </Modal>
        </>
    )
}

export default Matches;