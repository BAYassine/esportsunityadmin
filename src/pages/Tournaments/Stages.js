import React from 'react'
import { useState } from 'react'
import { Col, FormGroup, Row, Button, Input, Alert } from 'reactstrap'
import Label from 'reactstrap/lib/Label'
import Widget from '../../components/Widget/Widget'
import { AuthAxios } from '../../utils/AxiosInstances'
import { StageFormats } from '../../utils/constants'

const Stages = ({ stages, tournamentId, onUpdate }) => {

    const emptyStage = {
        name : null,
        order : stages.length + 1,
        format : null,
        rounds : null
    }

    const [ stage, setStage ] = useState(emptyStage)

    const handleChange = ({ target }) => {
        setStage({ ...stage, [target.name] : target.value });
    }

    const submit = () => {
        AuthAxios.post('admin/tournaments/' + tournamentId + '/stage', stage)
            .then(() => {
                onUpdate(stage)
                setStage(emptyStage)
            })
    }

    const onRemove = (stageOrder, stageId) => {
        if(window.confirm("Do you really on to remove Stage "+ stageOrder)){
            AuthAxios.delete(`admin/tournaments/${tournamentId}/stage/${stageId}`)
                .then(onUpdate)
        }
    }

    const tagStageAsCurrent = (stageId) => {
        AuthAxios.patch(`admin/tournaments/${tournamentId}/stage/${stageId}/current`)
            .then(onUpdate)
    }

    return (
        <>
            <Widget>
                <h2>New Stage</h2>
                <Row>
                    <Col>
                        <FormGroup>
                            <Label>Stage Name</Label>
                            <Input name="name" defaultValue={stage.name} onChange={handleChange}/>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label>Stage Order</Label>
                            <Input name="order" defaultValue={stage.order} onChange={handleChange}/>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup>
                            <Label>Stage Format</Label>
                            <Input type="select" name="format" defaultValue={stage.format} onChange={handleChange}>
                                <option value="SINGLE_ELIMINATION">Single Elimination</option>
                                <option value="DOUBLE_ELIMINATION">Double Elimination</option>
                                <option value="ROUND_ROBIN">Round Robin</option>
                                <option value="LEAGUE">League</option>
                            </Input>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label>Number of rounds</Label>
                            <Input type="number" name="rounds" defaultValue={stage.rounds} onChange={handleChange} />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-right">
                        <Button color="primary" onClick={submit}>Save</Button>
                    </Col>
                </Row>
            </Widget>

            <h2 className="mb-3">Tournament Stages</h2>
            <Row>
                {stages.map((stage, i) => ( 
                    <Col md={6} key={i}>
                        <Widget title={`Stage ${stage.order} : ${stage.name}`} close onClose={() => onRemove(stage.order, stage.id)}>
                            <h5><b>Format : </b>{StageFormats[stage.format]}</h5>
                            <h5><b>Rounds : </b>{stage.rounds}</h5>
                            {stage.current?
                                <Alert style={{ margin : "10% 0" }}>
                                    <div style={{ marginBottom : "0" }} className="h6 text-center">Current Stage</div>
                                </Alert>
                                :
                                <Button block color="primary" style={{ margin : "10%", width : "80%" }} onClick={() => tagStageAsCurrent(stage.id)}>Start</Button>
                            }
                        </Widget>
                    </Col>
                ))}
            </Row>
        </>
    )
}

export default Stages;