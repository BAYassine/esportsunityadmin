import React, { useEffect, useState } from "react";
import {
    Row,
    Col,
    Table,
    Progress,
    Button,
    UncontrolledButtonDropdown,
    DropdownMenu,
    DropdownToggle,
    DropdownItem,
    Input,
    Label,
    Badge,
} from "reactstrap";
import { Sparklines, SparklinesBars } from "react-sparklines";

import Widget from "../../components/Widget";
import { Link, useRouteMatch } from "react-router-dom";
import { AuthAxios } from "../../utils/AxiosInstances";
import moment from 'moment';
import { TournamentStatus } from "../../utils/constants";

const TournamentsTable = () => {

    const { url } = useRouteMatch();

    const [tournaments, setTournaments] = useState();

    useEffect(() => {
        AuthAxios.get('admin/tournaments/list')
            .then(({ data }) => setTournaments(data));
    }, [])

    return (
        <div>
            <div className="d-flex justify-content-between">
                <h2 className="page-title">
                    Tournaments - <span className="fw-semi-bold">List</span>
                </h2>
                <Link to={url + "/create"} >
                    <Button color="default"><i className="fa fa-plus mr-3 ml-2"></i>Create new Tournament</Button>
                </Link>
            </div>
            <Row>
                <Col>
                    <Widget
                        title={
                            <h5>
                                Table <span className="fw-semi-bold">Styles</span>
                            </h5>
                        }
                        settings
                        close
                    >
                        <Table striped>
                            <thead>
                                <tr className="fs-sm">
                                    <th className="hidden-sm-down">#</th>
                                    <th>Name</th>
                                    <th>Participation</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Teamsize</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {tournaments && tournaments.map((row, index) => (
                                    <tr key={row.id}>
                                        <td>{index + 1}</td>
                                        <td>
                                            {row.name}
                                        </td>
                                        <td>
                                            {row.participation}
                                        </td>
                                        <td>
                                            <small>
                                                <span className="text-muted fw-semi-bold">
                                                    {moment(row.startDate).format('DD MMM yyyy')}
                                                </span>
                                            </small>
                                        </td>
                                        <td>
                                            <small>
                                                <span className="text-muted fw-semi-bold">
                                                    {moment(row.endDate).format('DD MMM yyyy')}
                                                </span>
                                            </small>
                                        </td>
                                        <td className="text-muted">{row.teamsize}</td>
                                        <td>{TournamentStatus[row.status]}</td>
                                        <td>
                                            <Link to={`/tournaments/${row.id}`}><Button color="default">Details</Button></Link>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </Widget>
                </Col>
            </Row>
        </div>
    )
}

export default TournamentsTable;