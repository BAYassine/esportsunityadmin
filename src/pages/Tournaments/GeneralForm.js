import React, { useState, useEffect } from 'react';
import { Col, Row, Button, FormGroup, Input, Label } from 'reactstrap';
import DatePicker from "react-datepicker";

import Widget from '../../components/Widget/Widget';
import { AuthAxios } from '../../utils/AxiosInstances';

const { REACT_APP_SERVER_URL } = process.env;

const GeneralTournamentForm = ({ tournament : _tournament }) => {

    const [ games, setGames ] = useState();
    const [ tournament, setTournament ] = useState(_tournament);
    const [ cover, setCover ] = useState(`${REACT_APP_SERVER_URL}${_tournament.cover}`);

    useEffect(() => {
        AuthAxios.get('games/list')
            .then(({ data }) => setGames(data))
    }, [])

    const dateOrNow = (date) => {
        return date ? new Date(date) : new Date();
    }

    const handleChange = ({ target }) => {
        setTournament({ ...tournament, [target.name] : target.value });
    }

    const handleUpload = (event) => {
        const files = event.target.files;
        const name = event.target.name;
        if (files && files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                setCover(e.target.result)
            };            

            const formData = new FormData();
            formData.append('file', files[0]);
            AuthAxios.post('/media/upload', formData)
                .then(({ data }) => {
                    const _tournament = { ...tournament, [name] : data.name };
                    setTournament(_tournament);
                });

            reader.readAsDataURL(files[0]);
        }
    }

    const submit = () => {
        AuthAxios.put('admin/tournaments/update/'+_tournament.id, tournament)
    }

    return (
        <>
            <Widget title={
                <h5 className="mb-2">
                    Tournament <span className="fw-semi-bold">General info</span>
                </h5>
            }>
                <Row>
                    <Col>
                        <div className="tournament-cover">
                            {cover != null && (
                                <img src={cover} className="cover-img" />
                            )}
                            <Label className="cover-label">
                                <i className="fa fa-image"></i>
                                Choose a cover picture
                                <Input type="file" name="cover" onChange={handleUpload} />
                            </Label>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup>
                            <Label>Title</Label>
                            <Input name="name" bsSize="lg" type="text" placeholder="Name" 
                            defaultValue={tournament.name} onChange={handleChange} />
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label>Organizer</Label>
                            <Input type="text" bsSize="lg" name="organizer"
                            defaultValue={tournament.organizer} onChange={handleChange} />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md={6}>
                        <FormGroup>
                            <Label>Overview</Label>
                            <Input name="status" bsSize="lg" type="select" onChange={handleChange} defaultValue={tournament.status}>
                                <option selected disabled value="">Select tournament status</option>
                                <option value="REGISTRATION">Registration</option>
                                <option value="STARTING">Starting</option>
                                <option value="STARTING">Started</option>
                                <option value="GROUP_STAGE">Group Stage</option>
                                <option value="PLAYOFFS">PlayOffs</option>
                                <option value="SEMI_FINALS">Semi Finals</option>
                                <option value="FINALS">Finals</option>
                            </Input>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup>
                            <Label>Overview</Label>
                            <Input name="overview" className="overview" bsSize="lg" type="textarea"
                            defaultValue={tournament.overview} onChange={handleChange} />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup>
                            <Label>Start Date</Label>
                            <div>
                                <DatePicker className="form-control form-control-lg" selected={dateOrNow(tournament.startDate)}
                                    onChange={(date) => handleChange({ target: { name: "startDate", value: date } })} />
                            </div>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label>End Date</Label>
                            <div>
                                <DatePicker className="form-control form-control-lg" selected={dateOrNow(tournament.endDate)}
                                    onChange={(date) => handleChange({ target: { name: "endDate", value: date } })} />
                            </div>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup>
                            <Label>Places</Label>
                            <Input name="places" bsSize="lg" type="number" placeholder="Number of allowed registrations"
                            defaultValue={tournament.places} onChange={handleChange} />
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label>Teamsize</Label>
                            <Input name="teambsSize" bsSize="lg" type="text" placeholder="Post's source"
                            defaultValue={tournament.teamsize} onChange={handleChange} />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup>
                            <Label>Participation</Label>
                            <Input name="participation" bsSize="lg" type="select"
                             defaultValue={tournament.participation} onChange={handleChange}>
                                <option value="">Players should register as ?</option>
                                <option value="TEAM">Teams</option>
                                <option value="INDIVIDUAL">Individually</option>
                            </Input>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label>Tournament type</Label>
                            <Input name="type" bsSize="lg" type="select"
                                defaultValue={tournament.type} onChange={handleChange}>
                                <option value="">Select tournament type</option>
                                <option value="COMMUNITY">Everyone can register</option>
                                <option value="OFFICIAL">Only verified Teams/Players</option>
                            </Input>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup>
                            <Label>Publish Date (when tournament can be visible)</Label>
                            <div>
                                <DatePicker className="form-control form-control-lg" selected={dateOrNow(tournament.publishedOn)}
                                    onChange={(date) => handleChange({ target: { name: "publishedOn", value: date } })} />
                            </div>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label>Region</Label>
                            <Input name="region" bsSize="lg" type="text"
                            defaultValue={tournament.region} onChange={handleChange}></Input>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md={9}>
                        <FormGroup>
                            <Label>Game</Label>
                            <Input name="game" bsSize="lg" type="select" placeholder="Cover"
                                onChange={handleChange} value={tournament.game}>
                                <option value="">Select Game</option>
                                {games && games.map(game => (
                                    <option key={game.id} value={game.id}>{game.name}</option>
                                ))}
                            </Input>
                        </FormGroup>
                    </Col>
                </Row>
            </Widget>
            <Row>
                <Col></Col>
                <Col md={2}>
                    <Button color="primary" type="submit" block onClick={submit}>
                        Save
                    </Button>
                </Col>
            </Row>
        </>
    )
}

export default GeneralTournamentForm;