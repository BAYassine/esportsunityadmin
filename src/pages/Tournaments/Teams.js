import React from 'react'
import { useState, useEffect } from 'react'
import { Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import Button from 'reactstrap/lib/Button';
import Col from 'reactstrap/lib/Col';
import Widget from '../../components/Widget/Widget';
import { AuthAxios } from '../../utils/AxiosInstances'

const TournamentTeams = ({ tournamentId }) => {

    const [teams, setTeams] = useState();
    const [teamToKick, setTeamToKick] = useState();

    useEffect(() => {
        AuthAxios.get('tournaments/' + tournamentId + "/teams")
            .then(({ data }) => setTeams(data));
    }, [])

    const confirmKick = (team) => {
        setTeamToKick(team);
    }

    const kickTeam = () => {
        const name = teamToKick.name;
        alert(`Team '${name}' was kicked from the tournament`);
        setTeamToKick(null);
    }


    return (
        <>
            <h2>Teams</h2>
            {teams && (
                <Row>
                    {teams.map((team) => (
                        <Col key={team.id} md={4}>
                            <Widget>
                                <h5>{team.name}</h5>
                                <Row>
                                    <Col className="text-right">
                                        <Button color="danger" onClick={() => confirmKick(team)}>Kick</Button>
                                    </Col>
                                </Row>
                            </Widget>
                        </Col>
                    ))}
                </Row>
                
            )}

            <Modal isOpen={teamToKick} toggle={() => setTeamToKick(null)}>
                <ModalHeader toggle={() => setTeamToKick(null)}>Kick Team</ModalHeader>
                <ModalBody>
                    {teamToKick && 
                        <p>
                            Are you sure you want to kick Team : '{ teamToKick.name }' ?
                            <br/>
                            <small>This team won't be able to join this tournament unless you remove it from the tournament blacklist</small>
                        </p>
                    }
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => kickTeam()}>Yes</Button>{' '}
                    <Button color="secondary" onClick={() => setTeamToKick(null)}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </>
    )
}

export default TournamentTeams;