import React, { useEffect, useState } from 'react';
import { Col, Row, Button, FormGroup, Input, Label } from 'reactstrap';
import DatePicker from "react-datepicker";

import Widget from '../../components/Widget/Widget';
import { AuthAxios } from '../../utils/AxiosInstances';

import "react-datepicker/dist/react-datepicker.css";
import './Tournaments.scss';
import { useHistory } from 'react-router-dom';
import { TournamentStatus } from '../../utils/constants';

const TournamentsForm = () => {

    const history = useHistory();
    const [ games, setGames ] = useState();
    const [ tournament, setTournament ] = useState({
        name : "",
        organizer : "",
        overview : "",
        cover : "",
        startDate : new Date(),
        endDate : new Date(),
        entryClosesOn : new Date(),
        publishedOn : new Date(),
        places : null,
        status : TournamentStatus.REGISTRATION,
        teamsize : null,
        entryFee : null,
        region : "",
        participation : "TEAM" | "INDIVIDUAL",
        type : "COMMUNITY" | "OFFICIAL",
        game : null
    });
    const [ cover, setCover ] = useState();

    useEffect(() => {
        AuthAxios.get('games/list')
            .then(({ data }) => setGames(data))
    }, [])

    const handleChange = ({ target }) => {
        setTournament({ ...tournament, [target.name] : target.value });
    }

    const handleUpload = (event) => {
        const files = event.target.files;
        const name = event.target.name;
        if (files && files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                setCover(e.target.result)
            };            

            const formData = new FormData();
            formData.append('file', files[0]);
            AuthAxios.post('/media/upload', formData)
                .then(({ data }) => {
                    const _tournament = { ...tournament, [name] : data.name };
                    setTournament(_tournament);
                });

            reader.readAsDataURL(files[0]);
        }
    }

    const submit = (e) => {
        e.preventDefault();
        AuthAxios.post('admin/tournaments/create', tournament)
            .then(() => {
                history.push('/tournaments');
            })
    }
    
    return (
        <form onSubmit={submit}>
            <h2 className="page-title">
                News - <span className="fw-semi-bold">Create Tournament</span>
            </h2>
            <Widget title={
                <h5 className="mb-2">
                    Tournament <span className="fw-semi-bold">Information (Step 1)</span>
                </h5>
            }>
                
                <Row>
                    <Col>
                        <div className="tournament-cover">
                            {cover != null && (
                                <img src={cover} className="cover-img"/>
                            )}
                            <Label className="cover-label">
                                <i className="fa fa-image"></i>
                                Choose a cover picture
                                <Input type="file" name="cover" onChange={handleUpload}/>
                            </Label>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup>
                            <Label>Title</Label>
                            <Input name="name" size="lg" type="text" placeholder="Name" onChange={handleChange} />
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label>Organizer</Label>
                            <Input type="text" size="lg" name="organizer" onChange={handleChange}/>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup>
                            <Label>Overview</Label>
                            <Input name="overview" size="lg" type="textarea" onChange={handleChange} />
                        </FormGroup>
                    </Col>
                </Row>                
                <Row>
                    <Col>
                        <FormGroup controlId="team-country">
                            <Label>Start Date</Label>
                            <div>
                                <DatePicker className="form-control form-control-lg" selected={tournament.startDate}
                                    onChange={(date) => handleChange({ target : { name : "startDate", value : date }})} />
                            </div>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup controlId="team-country">
                            <Label>End Date</Label>
                            <div>
                                <DatePicker className="form-control form-control-lg" selected={tournament.endDate}
                                    onChange={(date) => handleChange({ target : { name : "endDate", value : date }})} />
                            </div>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup controlId="news-source">
                            <Label>Places</Label>
                            <Input name="places" size="lg" type="number" required placeholder="Number of allowed registrations" onChange={handleChange} />
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup controlId="news-source">
                            <Label>Teamsize</Label>
                            <Input name="teamsize" size="lg" type="text" placeholder="Post's source" onChange={handleChange} />
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup controlId="team-country">
                            <Label>Participation</Label>
                            <Input name="category" size="lg" type="select" onChange={handleChange}>
                                <option selected value="">Players should register as ?</option>
                                <option value="TEAM">Teams</option>
                                <option value="INDIVIDUAL">Individually</option>
                            </Input>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup controlId="team-country">
                            <Label>Tournament type</Label>
                            <Input name="category" size="lg" type="select" onChange={handleChange}>
                                <option selected value="">Select tournament type</option>
                                <option value="COMMUNITY">Everyone can register</option>
                                <option value="OFFICIAL">Only verified Teams/Players</option>
                            </Input>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup controlId="team-country">
                            <Label>Publish Date (when tournament can be visible)</Label>
                            <div>
                                <DatePicker className="form-control form-control-lg" selected={tournament.publishedOn}
                                onChange={(date) => handleChange({ target : { name : "publishedOn", value : date }})} />
                            </div>
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup controlId="team-country">
                            <Label>Region</Label>
                            <Input name="region" size="lg" type="text" onChange={handleChange}></Input>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col md={9}>
                        <FormGroup controlId="news-cover">
                            <Label>Game</Label>
                            <Input name="game" size="lg" type="select" placeholder="Cover" onChange={handleChange}>
                                <option selected value="">Select Game</option>
                                {games && games.map(game => (
                                    <option key={game.id} value={game.id}>{game.name}</option>
                                ))}
                            </Input>
                        </FormGroup>
                    </Col>
                </Row>
            </Widget>
            <Row>
                <Col></Col>
                <Col md={2}>
                    <Button color="primary" type="submit" block onClick={submit}>
                        Submit and Continue
                    </Button>
                </Col>
            </Row>
        </form>
    )
}

export default TournamentsForm;