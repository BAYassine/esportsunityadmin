import React, { useState } from 'react';
import { Col, Row, Button, FormGroup, Input, Label } from 'reactstrap';
import Widget from '../../components/Widget/Widget';
import { useHistory } from 'react-router-dom';
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

import "./News.scss";
import NewsSection from './NewsSection';
import { AuthAxios } from '../../utils/AxiosInstances';

const NewsForm = () => {
    const history = useHistory();

    const [news, setNews] = useState({
        title: null,
        author : null,
        publishedOn : new Date(),
        featured : false,
        category: null,
        source: null,
        briefing: null,
        photo: null,
        sections: [],
    });
    
    const [imagePreview, setImagePreview] = useState();

    const [sectionForm, setSectionForm] = useState({
        type : "",
        sections : []
    });

    const handleChange = (e) => {
        setNews({ ...news, [e.target.name]: e.target.value })
    }

    const handleSectionChange = (e) => {
        setSectionForm({ ...sectionForm, [e.target.name]: e.target.value })
    }

    const handleUpload = (event) => {
        const files = event.target.files;
        const name = event.target.name;
        if (files && files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                setImagePreview(e.target.result)
            };            

            const formData = new FormData();
            formData.append('file', files[0]);
            AuthAxios.post('/media/upload', formData)
                .then(({ data }) => {
                    const _news = { ...news, [name] : data.name };
                    setNews(_news);
                });

            reader.readAsDataURL(files[0]);
        }
    }

    const addSection = () => {
        setNews({ ...news, sections : news.sections.concat(sectionForm) });
        setSectionForm({ type : "", sections : [] })
    }

    const removeSection = (i) => {
        setNews({ ...news, sections : news.sections.filter((_, index) => index !==i) });
    }

    const updateSubSections = (subsection, order) => {
        const _news = { ...news }
        const _subsections = [...news.sections];
        _subsections[order] = subsection;
        _news.sections = _subsections;
        setNews(_news);
    }

    const submit = () => {
        AuthAxios.post(`/admin/news/create`, news)
            .then(() => history.push('/admin/news'))
    }

    return (
        <>
            <h2 className="page-title">
                News - <span className="fw-semi-bold">Create news</span>
            </h2>
            <Widget title={
                <h5 className="mb-2">
                    News <span className="fw-semi-bold">Information</span>
                </h5>
            }>
                <Row>
                    {!imagePreview && (
                        <div className="news-cover-placeholder">
                            <svg height="512pt" viewBox="-53 0 511 512" width="512pt" xmlns="http://www.w3.org/2000/svg"><path d="m117.832031 234.667969c-23.53125 0-42.664062-19.136719-42.664062-42.667969s19.132812-42.667969 42.664062-42.667969 42.667969 19.136719 42.667969 42.667969-19.136719 42.667969-42.667969 42.667969zm0-53.335938c-5.886719 0-10.664062 4.78125-10.664062 10.667969s4.777343 10.667969 10.664062 10.667969c5.890625 0 10.667969-4.78125 10.667969-10.667969s-4.777344-10.667969-10.667969-10.667969zm0 0"/><path d="m16.5 394.667969c-4.097656 0-8.191406-1.558594-11.308594-4.695313-6.25-6.25-6.25-16.382812 0-22.632812l64.855469-64.855469c14.589844-14.589844 38.335937-14.589844 52.90625 0l26.878906 26.882813 101.546875-101.546876c14.59375-14.59375 38.335938-14.59375 52.90625 0l96.855469 96.851563c6.25 6.25 6.25 16.382813 0 22.636719-6.25 6.25-16.382813 6.25-22.636719 0l-96.851562-96.855469c-2.089844-2.089844-5.589844-2.089844-7.660156 0l-112.851563 112.855469c-6.25 6.25-16.382813 6.25-22.636719 0l-38.183594-38.1875c-2.09375-2.089844-5.589843-2.089844-7.660156 0l-64.851562 64.851562c-3.117188 3.136719-7.210938 4.695313-11.308594 4.695313zm0 0"/><path d="m347.167969 512h-288c-32.363281 0-58.667969-26.304688-58.667969-58.667969v-394.664062c0-32.363281 26.304688-58.667969 58.667969-58.667969h288c32.363281 0 58.664062 26.304688 58.664062 58.667969v394.664062c0 32.363281-26.300781 58.667969-58.664062 58.667969zm-288-480c-14.699219 0-26.667969 11.96875-26.667969 26.667969v394.664062c0 14.699219 11.96875 26.667969 26.667969 26.667969h288c14.699219 0 26.664062-11.96875 26.664062-26.667969v-394.664062c0-14.699219-11.964843-26.667969-26.664062-26.667969zm0 0"/></svg>
                            <div>News Cover Image</div>
                        </div>
                    )}
                    {imagePreview != null && <img src={imagePreview} className="news-cover" alt="cover"/>}
                    <Col>
                        <Row>
                            <Col md={9}>
                                <FormGroup controlId="news-title">
                                    <Label>Title</Label>
                                    <Input name="title" size="lg" type="text" placeholder="Title" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup check style={{ marginTop : 40 }}>
                                    <Label check>
                                    <Input type="checkbox" name="featured"/>{' '}Featured</Label>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup controlId="news-briefing">
                                    <Label>Briefing</Label>
                                    <Input name="briefing" size="lg" type="textarea" placeholder="Enter a small briefing" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup controlId="news-source">
                                    <Label>Author</Label>
                                    <Input name="author" size="lg" type="text" placeholder="Author's name" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup controlId="news-source">
                                    <Label>Source</Label>
                                    <Input name="source" size="lg" type="text" placeholder="Post's source" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup controlId="team-country">
                                    <Label>Publish Date</Label>
                                    <div>
                                        <DatePicker className="form-control form-control-lg" selected={news.publishedOn}
                                        onChange={(date) => handleChange({ target : { name : "publishedOn", value : date }})} />
                                    </div>
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup controlId="team-country">
                                    <Label>Category</Label>
                                    <Input name="category" size="lg" type="select" onChange={handleChange}>
                                        <option selected value="">Select category</option>
                                        <option value="ESPORTS">ESPORTS</option>
                                        <option value="GAMES">GAMES</option>
                                        <option value="EVENTS">EVENTS</option>
                                        <option value="TOURNAMENTS">TOURNAMENTS</option>
                                        <option value="REPLAYS">REPLAYS</option>
                                        <option value="ANNOUNCEMENTS">ANNOUNCEMENTS</option>
                                    </Input>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={9}>
                                <FormGroup controlId="news-cover">
                                    <Label>Cover photo</Label>
                                    <Input name="cover" size="lg" type="file" placeholder="Cover" onChange={handleUpload} />
                                </FormGroup>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Widget>

            <Widget title={
                <h5 className="mb-2">
                    News <span className="fw-semi-bold">Content</span>
                </h5>
            }>
                {news.sections.map((section, i) => (
                    <NewsSection key={i} section={section} order={i} onChange={updateSubSections}
                        onRemove={() => removeSection(i)}/>
                ))}
                <Row>
                    <Col md={{ offset : 3, size : 2 }}>
                        <Input name="type" size="lg" type="select" onChange={handleSectionChange} value={sectionForm.type}>
                            <option selected value="">Select type</option>
                            <option value="GROUP">Section Group</option>
                            <option value="PARAGRAPH">Paragraph</option>
                            <option value="MEDIA">Media</option>
                            <option value="QUOTE">Quote</option>
                            <option value="BRIEFING">Briefing</option>
                        </Input>
                    </Col>
                    <Col md="2">
                        <Button block color="default" disabled={!sectionForm.type} onClick={addSection}>Add section</Button>
                    </Col>
                </Row>
            </Widget>
            <Row>
                <Col></Col>
                <Col md={2}>
                    <Button color="primary" type="submit" block onClick={submit}>
                        Submit
                    </Button>
                </Col>
            </Row>
        </>
    )
}

export default NewsForm;