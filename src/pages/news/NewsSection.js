import React, { useState, useEffect, useRef } from "react";
import { Row, Col, FormGroup, Label, Input, Button } from "reactstrap";
import { AuthAxios } from "../../utils/AxiosInstances";

const sectionProps = {
    PARAGRAPH: {
        title: "",
        content: ""
    }
}

const NewsSection = (props) => {

    const [section, setSection] = useState(props.section);

    const [sectionForm, setSectionForm] = useState({
        type: "",
        sections: []
    });

    const handleUpload = (event) => {
        const files = event.target.files;
        if (files && files[0]) {
            var reader = new FileReader();

            // reader.onload = function (e) {
            //     const _section = { ...section, [target]: e.target.result };
            //     setSection(_section);
            //     props.onChange(_section, props.order);
            // };            

            const formData = new FormData();
            formData.append('file', files[0]);
            AuthAxios.post('/media/upload', formData)
                .then(({ data }) => {
                    const _section = { ...section, file : data.name };
                    setSection(_section);
                    props.onChange(_section, props.order);
                });
            reader.readAsDataURL(files[0]);
        }
    }

    const handleChange = (e) => {
        const _section = { ...section, [e.target.name]: e.target.value };
        setSection(_section);
        props.onChange(_section, props.order);
    }

    const handleSectionChange = (e) => {
        setSectionForm({ ...sectionForm, [e.target.name]: e.target.value })
    }

    const addSection = () => {
        const _section = { ...section, sections: section.sections.concat(sectionForm) }
        setSection(_section);
        props.onChange(_section, props.order);
        setSectionForm({ type: "", alignment: sectionForm.alignment, sections: [] })
    }

    const removeSection = (i) => {
        const _section = { ...section, sections: section.sections.filter((_, index) => index != i) }
        setSection(_section);
        props.onChange(_section, props.order);
    }

    const toggleAlignment = () => {
        const alignment = sectionForm.alignment == "V" ? "H" : "V";
        setSectionForm({ ...sectionForm, alignment });
        const _section = { ...section, alignment };
        setSection(_section)
        props.onChange(_section, props.order);
    }

    const updateSubSections = (subsection, order) => {
        console.log(props.order);
        const _section = { ...section }
        const _subsections = [...section.sections];
        _subsections[order] = subsection;
        _section.sections = _subsections;
        setSection(_section);
        props.onChange(_section, props.order);
    }

    const sectionContent = () => {

        switch (section.type) {
            case "PARAGRAPH":
                return (
                    <>
                        <Row>
                            <Col>
                                <h5><strong>Paragraph section</strong></h5>
                            </Col>
                            <Col md="4" className="text-right">
                                <Button color="inverse" onClick={props.onRemove}>
                                    <i className="fa fa-times" style={{ marginRight: 10 }}></i>
                                    Remove section
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label>Paragraph title</Label>
                                    <Input name="title" size="lg" type="text" placeholder="Title" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label>Content</Label>
                                    <Input name="content" size="lg" type="textarea" placeholder="Enter name" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                    </>
                )
            case "MEDIA":
                return (
                    <>
                        <Row>
                            <Col>
                                <h5><strong>Media section</strong></h5>
                            </Col>
                            <Col md="4" className="text-right">
                                <Button color="inverse" onClick={props.onRemove}>
                                    <i className="fa fa-times" style={{ marginRight: 10 }}></i>
                                    Remove section
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label>Title</Label>
                                    <Input name="title" size="lg" type="text" placeholder="Media title" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label>File</Label>
                                    <Input name="file" size="lg" type="file" onChange={handleUpload} />
                                </FormGroup>
                            </Col>
                            <Col sm={1}><h3>OR</h3></Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label>Link</Label>
                                    <Input name="link" size="lg" type="text" placeholder="wwww.example.com/example" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label>Source</Label>
                                    <Input name="source" size="lg" type="text" placeholder="Example : Name of social network or Website" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                    </>
                )
            case "QUOTE":
                return (
                    <>
                        <Row>
                            <Col>
                                <h5><strong>Quote section</strong></h5>
                            </Col>
                            <Col md="4" className="text-right">
                                <Button color="inverse" onClick={props.onRemove}>
                                    <i className="fa fa-times" style={{ marginRight: 10 }}></i>
                                    Remove section
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label>Quote</Label>
                                    <Input name="quote" size="lg" type="textarea" placeholder="Enter the quote here" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label>Said By</Label>
                                    <Input name="said_by" size="lg" type="text" placeholder="Who said it" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label>Date</Label>
                                    <Input name="date" size="lg" type="date" placeholder="When" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                    </>
                )
            case "BRIEFING":
                return (
                    <>
                        <Row>
                            <Col>
                                <h5><strong>Briefing section</strong></h5>
                            </Col>
                            <Col md="4" className="text-right">
                                <Button color="inverse" onClick={props.onRemove}>
                                    <i className="fa fa-times" style={{ marginRight: 10 }}></i>
                                    Remove section
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label>Content</Label>
                                    <Input name="content" size="lg" type="textarea" placeholder="Enter name" onChange={handleChange} />
                                </FormGroup>
                            </Col>
                        </Row>
                    </>
                )
            case "GROUP":
                return (
                    <div>
                        <Row>
                            <Col>
                                <h5><strong>Section group</strong></h5>
                            </Col>
                            <Col md={3} className="text-right">
                                <Button color="default" onClick={toggleAlignment}>
                                    {sectionForm.alignment == "V" ?
                                        <>
                                            <i className="fa fa-ellipsis-v" style={{ marginRight: 10 }}></i>
                                                Vertical alignment
                                            </>
                                        :
                                        <>
                                            <i className="fa fa-ellipsis-h" style={{ marginRight: 10 }}></i>
                                                Horizontal alignement
                                            </>
                                    }
                                </Button>
                            </Col>
                        </Row>
                        <div className={"sections-wrapper" + (sectionForm.alignment == "V" ? "" : " flex-h")}>
                            {section.sections.map((section, i) => (
                                <NewsSection key={i} section={section} alignment={sectionForm.alignment}
                                    order={i} onChange={updateSubSections} onRemove={() => removeSection(i)} />
                            ))}
                        </div>
                        <Row>
                            <Col md={{ offset: (props.alignment == "H" ? 0 : 3), size: props.alignment == "H" ? 6 : 2 }}>
                                <Input name="type" size="lg" type="select" onChange={handleSectionChange} value={sectionForm.type}>
                                    <option selected value="">Select type</option>
                                        <option value="GROUP">Section Group</option>
                                    <option value="PARAGRAPH">Paragraph</option>
                                    <option value="MEDIA">Media</option>
                                    <option value="QUOTE">Quote</option>
                                    <option value="BRIEFING">Briefing</option>
                                </Input>
                            </Col>
                            <Col md={props.alignment == "H" ? 6 : 2}>
                                <Button block color="default" disabled={!sectionForm.type} onClick={addSection}>Add section</Button>
                            </Col>
                        </Row>
                    </div>
                )
            default:
                return (
                    <></>
                )
        }
    }

    return (
        <div className="news-section">
            {sectionContent()}
        </div>
    )
}

export default NewsSection;