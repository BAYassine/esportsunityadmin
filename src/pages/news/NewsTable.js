import React, { useEffect, useState } from 'react';
import {
  Row,
  Col,
  Table,
  Button,
} from "reactstrap";

import Widget from "../../components/Widget";
// import s from"./Games.module.scss";
import moment from "moment";
import { Link, useRouteMatch } from 'react-router-dom';
import { AuthAxios } from '../../utils/AxiosInstances';

const NewsTable = () => {

  const { url } = useRouteMatch();
  const [news, setNews] = useState(null);
  const [featuredNews, setFeaturedNews] = useState(null);

  const [ page, setPage ] = useState(1);
  const [ items, setItems ] = useState(15);

  useEffect(() => {
    AuthAxios.get(`admin/news/featured`)
      .then(({ data }) => setFeaturedNews(data));
    AuthAxios.get(`admin/news/list`)
      .then(({ data }) => setNews(data));
  }, []);

  const goToPage =  (_page) => {
    setPage(_page);
    AuthAxios.get(`admin/news/list?page=${_page}&items=${items}`)
      .then(({ data }) => setNews(data));
  }

  const changeToRegular = (id) => {
    AuthAxios.put('admin/news/update', { id, featured : false })
      .then(() => {
        AuthAxios.get(`admin/news/featured`)
          .then(({ data }) => setFeaturedNews(data));
        AuthAxios.get(`admin/news/list?page=${page}&items=${items}`)
          .then(({ data }) => setNews(data));
      })
  }

  return (
    <div>
      <div className="d-flex justify-content-between">
        <h2 className="page-title">
          News - <span className="fw-semi-bold">List</span>
        </h2>
        <Link to={url + "/create"} >
          <Button color="primary"><i className="fa fa-plus mr-3 ml-2"></i>Create news</Button>
        </Link>
      </div>
      <Row>
        <Col>
          <Widget
              title={
                <h5>
                  Featured <span className="fw-semi-bold">News</span>
                </h5>
              }
            settings
            close
            // bodyClass={s.mainTableWidget}
          >
            <Table striped>
              <thead>
                <tr className="fs-sm">
                  <th className="hidden-sm-down">#</th>
                  <th>Title</th>
                  <th className="hidden-sm-down">Category</th>
                  <th className="hidden-sm-down">Publish Date</th>
                  <th>Created By</th>
                  {/* <th className="hidden-sm-down">Status</th> */}
                </tr>
              </thead>
              <tbody>
                {featuredNews && featuredNews.map((row, i) => (
                  <tr key={row.id}>
                    <td>{i + 1}</td>
                    <td>
                      {row.title}
                    </td>
                    <td>
                      {row.category}
                    </td>
                    <td>{moment(news.publishedOn).format('MMMM Do, YYYY')}</td>
                    <td>
                      {row.createdBy?.name}
                    </td>
                    <td className="width-150">
                        <Button color="primary" onClick={() => changeToRegular(row.id)}>To Regular</Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            {/* <div className="clearfix">
              <div className="float-right">
                <Button color="default" className="mr-2" size="sm">
                  Send to...
                </Button>
              </div>
              <p>Basic table with styled content</p>
            </div> */}
          </Widget>
        </Col>
      </Row>
      <Row>
        <Col>
          <Widget
              title={
                <h5>
                  Regular <span className="fw-semi-bold">News</span>
                </h5>
              }
            settings
            close
            // bodyClass={s.mainTableWidget}
          >
            <Table striped>
              <thead>
                <tr className="fs-sm">
                  <th className="hidden-sm-down">#</th>
                  <th>Title</th>
                  <th className="hidden-sm-down">Category</th>
                  <th className="hidden-sm-down">Publish Date</th>
                  <th>Created By</th>
                  {/* <th className="hidden-sm-down">Status</th> */}
                </tr>
              </thead>
              <tbody>
                {news && news.map((row, i) => (
                  <tr key={row.id}>
                    <td>{i + 1}</td>
                    <td>
                      {row.title}
                    </td>
                    <td>
                      {row.category}
                    </td>
                    <td>{moment(news.publishedOn).format('MMMM Do, YYYY')}</td>
                    <td>
                      {row.createdBy?.name}
                    </td>
                    {/* <td className="width-150"></td> */}
                  </tr>
                ))}
              </tbody>
            </Table>
            {/* <div className="clearfix">
              <div className="float-right">
                <Button color="default" className="mr-2" size="sm">
                  Send to...
                </Button>
              </div>
              <p>Basic table with styled content</p>
            </div> */}
          </Widget>
        </Col>
      </Row>

    </div>
  )
}

export default NewsTable;