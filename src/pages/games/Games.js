import React, { useEffect, useState } from 'react';
import {
  Row,
  Col,
  Table,
  Button,
} from "reactstrap";

import Widget from "../../components/Widget";
// import s from"./Games.module.scss";
import axios from 'axios';
import config from '../../config';
import { Link, useRouteMatch } from 'react-router-dom';

const Games = () => {

  const { url } = useRouteMatch();
  const [games, setGames] = useState(null);

  useEffect(() => {
    axios.get(`${config.API_URL}/games/list`)
      .then(({ data }) => setGames(data));
  }, []);

  return (
    <div>
      <div className="d-flex justify-content-between">
        <h2 className="page-title">
          Games - <span className="fw-semi-bold">List</span>
        </h2>
        <Link to={url + "/create"} >
          <Button color="primary"><i className="fa fa-plus mr-3 ml-2"></i>Create new game</Button>
        </Link>
      </div>
      <Row>
        <Col>
          <Widget
            settings
            close
            // bodyClass={s.mainTableWidget}
          >
            <Table striped>
              <thead>
                <tr className="fs-sm">
                  <th className="hidden-sm-down">#</th>
                  <th>Logo</th>
                  <th className="hidden-sm-down">Name</th>
                  <th className="hidden-sm-down">Category</th>
                  <th>Developer</th>
                  <th>API</th>
                  {/* <th className="hidden-sm-down">Status</th> */}
                </tr>
              </thead>
              <tbody>
                {games && games.map((row, i) => (
                  <tr key={row.id}>
                    <td>{i + 1}</td>
                    <td>
                      <img
                        className="img-rounded"
                        src={`${process.env.REACT_APP_SERVER_URL}${row.icon}`}
                        alt=""
                        height="50"
                      />
                    </td>
                    <td>
                      {row.name}
                    </td>
                    <td className="text-muted">{row.categories && row.categories.join(',')}</td>
                    <td>
                      {row.developer}
                    </td>
                    <td>
                      {row.api && row.api.name}
                    </td>
                    {/* <td className="width-150"></td> */}
                  </tr>
                ))}
              </tbody>
            </Table>
            {/* <div className="clearfix">
              <div className="float-right">
                <Button color="default" className="mr-2" size="sm">
                  Send to...
                </Button>
              </div>
              <p>Basic table with styled content</p>
            </div> */}
          </Widget>
        </Col>
      </Row>

    </div>
  )
}

export default Games;