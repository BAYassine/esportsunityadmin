import React, { useCallback, useState } from 'react';
import { Col, Row, Form, Button, FormGroup, FormText, Input, Label } from 'reactstrap';
import "./Games.scss";
import Widget from '../../components/Widget/Widget';
import axios from 'axios';
import config from '../../config';
import { useHistory } from 'react-router-dom';
import ImageUploader from "react-images-upload";
import { AuthAxios } from '../../utils/AxiosInstances';

const GameForm = () => {

    const history = useHistory();
    const [logo, setLogo] = useState();
    const [game, setGame] = useState({
        name : null,
        developer : null,
        overview : null,
        categories : null,
        api_name : null,
        api_token : null,
        website : null,
        nickname_label : null,
        rank_label : null,
        portrait : null,
        icon : null,
        transparent : null,
        cover : null
    })

    const [saving, setSaving] = useState(false)

    const onDrop = (pictureFiles, pictureDataURLs) => {
        setLogo(pictureFiles[0])
    }
    
    const handleChange = (e) => {
        setGame({ ...game, [e.target.name] : e.target.value })
    }

    const handleFile = (e) => {
        setGame({ ...game, [e.target.name] : e.target.files[0] });
    }

    const submit = () => {
        setSaving(true);
        const newGame = {...game}
        newGame.api = {
            name : game.api_name,
            token : game.api_token
        }
        delete newGame.icon;
        delete newGame.cover;
        delete newGame.portrait;
        delete newGame.transparent;
        delete newGame.api_name;
        delete newGame.api_token;
        const formData = new FormData();
        formData.append('icon', game.icon)
        formData.append('portrait', game.portrait)
        formData.append('cover', game.cover)
        formData.append('transparent', game.transparent)
        formData.append('game', JSON.stringify(newGame));
        AuthAxios.post(`/admin/games/create`, formData, { headers : { 'content-type' : 'multipart/form-data' } })
            .then(() => {
                setSaving(false);
                history.push('/games')
            })
    }

    return (
        <>
            <h2 className="page-title">
                Game - <span className="fw-semi-bold">Create new game</span>
            </h2>
            <Widget title={
                <h5 className="mb-2">
                    Game <span className="fw-semi-bold">Informations</span>
                </h5>
            }>
                <div className="mb-5">
                    <Row>
                        <Col>
                            <FormGroup controlId="game-image-2x3">
                                <Label>Portrait 2x3 (Home)</Label>
                                <div><input type="file" name="portrait" onChange={handleFile}/></div>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup controlId="game-icon">
                                <Label>Game icon</Label>
                                <div><input type="file" name="icon" onChange={handleFile}/></div>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup controlId="game-list-image">
                                <Label>Transparent Image (Games List)</Label>
                                <div><input type="file" name="transparent" onChange={handleFile}/></div>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormGroup controlId="game-image-3x2">
                                <Label>Game cover (3x1)</Label>
                                <div><input type="file" name="cover" onChange={handleFile}/></div>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={9}>
                            <FormGroup controlId="game-name">
                                <Label>Game name</Label>
                                <Input name="name" size="lg" type="text" placeholder="Enter name" className={'teamname_field'} onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormGroup controlId="game-overview">
                                <Label>Overview</Label>
                                <Input name="overview" size="lg" type="textarea" placeholder="Game overbiew" className={'teamabout_field'} onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormGroup controlId="game-developer">
                                <Label>Developer</Label>
                                <Input name="developer" size="lg" type="text" placeholder="Game developer name" onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup controlId="game-website">
                                <Label>Website</Label>
                                <Input name="website" size="lg" type="url" placeholder="Enter website" onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormGroup controlId="game-nickname">
                                <Label>Nickname label</Label>
                                <Input name="nickname_label" size="lg" placeholder="Game nickname label" onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup controlId="rank-label">
                                <Label>Rank Label</Label>
                                <Input name="rank_label" size="lg" placeholder="Enter rank label" onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <h5 className="mb-2 mt-5">
                        API <span className="fw-semi-bold">Informations</span>
                    </h5>
                    <Row>
                        <Col md={8}>
                            <FormGroup controlId="game-nickname">
                                <Label>Name</Label>
                                <Input name="api_name" size="lg" placeholder="API Name" onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={8}>
                            <FormGroup controlId="game-nickname">
                                <Label>Token</Label>
                                <Input name="api_name" size="lg" placeholder="API Token" onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                    </Row>
                </div>  
                <Row>
                    <Col></Col>
                    <Col md={2}>
                        <Button color="primary" type="submit" block onClick={submit} disabled={saving}>
                            {saving ?
                                <span><i className="fa fa-circle-o-notch fa-spin"></i> Saving</span>
                                :
                                <span>Submit</span>
                            }
                        </Button>
                    </Col>
                </Row>
            </Widget>
        </>
    )
}

export default GameForm;